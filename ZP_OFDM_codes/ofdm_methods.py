import numpy as np
import ofdm
import ofdmclass
import ofdm_func
from sklearn import preprocessing
import tensorflow as tf
from sklearn.preprocessing import StandardScaler
import pdb
from cmath import sqrt
from scipy.linalg import norm
import matplotlib.pyplot as plt



def ZPZJ(bits_piloto_all, bits_all, N, K, mu, mapping_table,demapping_table, SNRdb, nonlinearity, channelResponse):

    pilot_symb = ofdmclass.symbol_block(bits_piloto_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_RX, OFDM_data_piloto]= ofdm_func.symb_transmission2(pilot_symb, channelResponse, True, nonlinearity)

    OFDM_demod_piloto = ofdm.DFT(OFDM_RX)
    Hest_ls  = (OFDM_demod_piloto/OFDM_data_piloto),
    oi = ofdm.IDFT(Hest_ls)
    oi = np.reshape(oi, (N,))
    # h_est = channelResponse
    h_est = oi[0:len(channelResponse)]


    G_barra = ofdm.inversa_MMSE(h_est, N+K, K, SNRdb)

    data_symb = ofdmclass.symbol_block(bits_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_RX, OFDM_data_symb] = ofdm_func.symb_transmission2(data_symb, channelResponse,False, nonlinearity)

    OFDM_RX_ok = np.dot(G_barra, OFDM_RX)
    equalized_Hest_ls = ofdm.DFT(OFDM_RX_ok)

    QAM_est = ofdm_func.symb_ber(data_symb, demapping_table,equalized_Hest_ls,bits_all)
    return QAM_est

def ZPZJ_1(model1,bits_piloto_all, bits_all, N, K, mu, mapping_table,demapping_table, SNRdb, nonlinearity, channelResponse):

    pilot_symb = ofdmclass.symbol_block(bits_piloto_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_RX, OFDM_data_piloto]= ofdm_func.symb_transmission2(pilot_symb, channelResponse, True, nonlinearity)

    OFDM_demod_piloto = ofdm.DFT(OFDM_RX)
    Hest_ls  = (OFDM_demod_piloto/OFDM_data_piloto)

    H_LS = ofdm.block_real_imag(Hest_ls)
    scale = norm(H_LS)
    # Encontrando estimativa melhor do canal através da CE Net:
    H_hat = model1.predict(np.reshape(H_LS/scale, (1, 2 * N)))
    H_hat_ok = []
    for i in range(N):
        H_hat_ok.append(H_hat[0, i] + 1j * H_hat[0, i + N])
    H_hat_ok = np.asarray(H_hat_ok)*scale

    oi = ofdm.IDFT(H_hat_ok)
    oi = np.reshape(oi, (N,))
    # h_est = channelResponse
    h_est = oi[0:len(channelResponse)]


    G_barra = ofdm.inversa_MMSE(h_est, N+K, K, SNRdb)

    data_symb = ofdmclass.symbol_block(bits_all, N, K, mu, mapping_table, SNRdb )
    [OFDM_RX, OFDM_data_symb] = ofdm_func.symb_transmission2(data_symb, channelResponse,False, nonlinearity)

    OFDM_RX_ok = np.dot(G_barra, OFDM_RX)
    equalized_Hest_ls = ofdm.DFT(OFDM_RX_ok)

    QAM_est = ofdm_func.symb_ber(data_symb, demapping_table,equalized_Hest_ls,bits_all)
    return QAM_est



def ZPZJ_2(model1, model, bits_piloto_all, bits_all, N, K, mu, mapping_table,demapping_table, SNRdb, nonlinearity, channelResponse):

    pilot_symb = ofdmclass.symbol_block(bits_piloto_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_RX, OFDM_data_piloto]= ofdm_func.symb_transmission2(pilot_symb, channelResponse, True, nonlinearity)

    OFDM_demod_piloto = ofdm.DFT(OFDM_RX)
    Hest_ls  = (OFDM_demod_piloto/OFDM_data_piloto)

    H_LS = ofdm.block_real_imag(Hest_ls)
    scale = norm(H_LS)
    # Encontrando estimativa melhor do canal através da CE Net:
    H_hat = model1.predict(np.reshape(H_LS / scale, (1, 2 * N)))
    H_hat_ok = []
    for i in range(N):
        H_hat_ok.append(H_hat[0, i] + 1j * H_hat[0, i + N])
    H_hat_ok = np.asarray(H_hat_ok) * scale

    oi = ofdm.IDFT(H_hat_ok)
    oi = np.reshape(oi, (N,))
    # h_est = channelResponse
    h_est = oi[0:len(channelResponse)]


    G_barra = ofdm.inversa_MMSE(h_est, N+K, K, SNRdb)

    data_symb = ofdmclass.symbol_block(bits_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_RX, OFDM_data_symb] = ofdm_func.symb_transmission2(data_symb, channelResponse,False, nonlinearity)

    OFDM_RX_ok = np.dot(G_barra, OFDM_RX)
    equalized_Hest_ls = ofdm.DFT(OFDM_RX_ok)

    y = (ofdm.block_real_imag(equalized_Hest_ls))
    oi2 = np.reshape(y, (1, -1))
    mrscaler = norm(oi2)
    y_hat = model.predict(np.reshape(oi2 / mrscaler, (1, 2 * N)))

    u_hat = []
    for i in range(N):
        u_hat.append(y_hat[0, i] + 1j * y_hat[0, i + N])
    # print(u_hat)
    # pdb.set_trace()
    u_hat = np.asarray(u_hat) * mrscaler

    QAM_est = ofdm_func.symb_ber(data_symb, demapping_table,u_hat,bits_all)
    return QAM_est


def MR(model1,model,bits_piloto_all, bits_all, N, K, mu, mapping_table,demapping_table, SNRdb , nonlinearity, channelResponse):

    pilot_symb = ofdmclass.symbol_block(bits_piloto_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_demod_piloto, OFDM_data_piloto, OFDM_TX_piloto]= ofdm_func.symb_transmission(pilot_symb, channelResponse, True, nonlinearity,[])
    Hest_ls  = (OFDM_demod_piloto/OFDM_data_piloto)

    H_LS = ofdm.block_real_imag(Hest_ls)
    scale = norm(H_LS)
    # Encontrando estimativa melhor do canal através da CE Net:
    H_hat = model1.predict(np.reshape(H_LS / scale, (1, 2 * N)))
    H_hat_ok = []
    for i in range(N):
        H_hat_ok.append(H_hat[0, i] + 1j * H_hat[0, i + N])
    H_hat_ok = np.asarray(H_hat_ok) * scale

    data_symb = ofdmclass.symbol_block(bits_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_demod, OFDM_data_symb,ant] = ofdm_func.symb_transmission(data_symb, channelResponse,False, nonlinearity,OFDM_TX_piloto)
    equalized_Hest_cenet = ofdm.equalizer(OFDM_demod, H_hat_ok)

    y = (ofdm.block_real_imag(equalized_Hest_cenet))
    oi2 = np.reshape(y, (1, -1))
    mrscaler = norm(oi2)
    y_hat = model.predict(np.reshape(oi2/mrscaler, (1, 2 * N)))


    u_hat = []
    for i in range(N):
        u_hat.append(y_hat[0, i] + 1j * y_hat[0, i + N])
    # print(u_hat)
    # pdb.set_trace()
    u_hat = np.asarray(u_hat)*mrscaler

    QAM_est = ofdm_func.symb_ber(data_symb, demapping_table,u_hat,bits_all)
    return QAM_est

def Net1(model1,bits_piloto_all, bits_all, N, K, mu, mapping_table,demapping_table, SNRdb, nonlinearity, channelResponse):

    pilot_symb = ofdmclass.symbol_block(bits_piloto_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_demod_piloto, OFDM_data_piloto, OFDM_TX_piloto]= ofdm_func.symb_transmission(pilot_symb, channelResponse, True, nonlinearity,[])
    Hest_ls  = (OFDM_demod_piloto/OFDM_data_piloto)

    H_LS = ofdm.block_real_imag(Hest_ls)
    scale=norm(H_LS)
    # Encontrando estimativa melhor do canal através da CE Net:
    H_hat = model1.predict(np.reshape(H_LS/scale, (1, 2 * N)))
    H_hat_ok = []
    for i in range(N):
        H_hat_ok.append(H_hat[0, i] + 1j * H_hat[0, i + N])
    H_hat_ok = np.asarray(H_hat_ok)*scale

    # plt.plot(np.fft.fft(channelResponse, N), '-bo')
    # plt.plot(Hest_ls, '-r+')
    # plt.plot(H_hat_ok, '-mx')
    # import pdb
    # pdb.set_trace()
    data_symb = ofdmclass.symbol_block(bits_all, N, K, mu, mapping_table, SNRdb )
    [OFDM_demod, OFDM_data_symb,ant] = ofdm_func.symb_transmission(data_symb, channelResponse,False, nonlinearity,OFDM_TX_piloto)
    equalized_Hest_ls = ofdm.equalizer(OFDM_demod, H_hat_ok)
    QAM_est = ofdm_func.symb_ber(data_symb, demapping_table,equalized_Hest_ls,bits_all)
    return QAM_est

def CE(model1,bits_piloto_all, bits_all, N, K, mu, mapping_table,demapping_table, SNRdb , nonlinearity, channelResponse):

    pilot_symb = ofdmclass.symbol_block(bits_piloto_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_demod_piloto, OFDM_data_piloto, OFDM_TX_piloto]= ofdm_func.symb_transmission(pilot_symb, channelResponse, True, nonlinearity,[])
    Hest_ls  = (OFDM_demod_piloto/OFDM_data_piloto)

    H_LS = ofdm.block_real_imag(Hest_ls)
    # Encontrando estimativa melhor do canal através da CE Net:
    H_hat = model1.predict(np.reshape(H_LS, (1, 2 * N)))
    H_hat_ok = []
    for i in range(N):
        H_hat_ok.append(H_hat[0, i] + 1j * H_hat[0, i + N])
    H_hat_ok = np.asarray(H_hat_ok)

    # plt.plot(np.fft.fft(channelResponse, N), '-bo')
    # plt.plot(Hest_ls, '-r+')
    # plt.plot(H_hat_ok, '-mx')
    # import pdb
    # pdb.set_trace()
    data_symb = ofdmclass.symbol_block(bits_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_demod, OFDM_data_symb,ant] = ofdm_func.symb_transmission(data_symb, channelResponse,False, nonlinearity,OFDM_TX_piloto)
    equalized_Hest_ls = ofdm.equalizer(OFDM_demod, H_hat_ok)
    QAM_est = ofdm_func.symb_ber(data_symb, demapping_table,equalized_Hest_ls,bits_all)
    return QAM_est

def LS(bits_piloto_all, bits_all, N, K, mu, mapping_table,demapping_table, SNRdb, nonlinearity, channelResponse):

    pilot_symb = ofdmclass.symbol_block(bits_piloto_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_demod_piloto, OFDM_data_piloto, OFDM_TX_piloto]= ofdm_func.symb_transmission(pilot_symb, channelResponse, True, nonlinearity,[])
    Hest_ls  = (OFDM_demod_piloto/OFDM_data_piloto),

    data_symb = ofdmclass.symbol_block(bits_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_demod, OFDM_data_symb,ant] = ofdm_func.symb_transmission(data_symb, channelResponse,False, nonlinearity,OFDM_TX_piloto)
    equalized_Hest_ls = ofdm.equalizer(OFDM_demod, Hest_ls)
    QAM_est = ofdm_func.symb_ber(data_symb, demapping_table,equalized_Hest_ls,bits_all)
    return QAM_est

def LMMSE(bits_piloto_all, bits_all, N, K, mu, mapping_table,demapping_table, SNRdb , nonlinearity, channelResponse):

    pilot_symb = ofdmclass.symbol_block(bits_piloto_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_demod_piloto, OFDM_data_piloto, OFDM_TX_piloto]= ofdm_func.symb_transmission(pilot_symb, channelResponse, True, nonlinearity,[])

    Hest_wcp = ofdm.MMSEchannelEstimate(OFDM_demod_piloto,OFDM_data_piloto,N,N,channelResponse,SNRdb)

    data_symb = ofdmclass.symbol_block(bits_all, N, K, mu, mapping_table, SNRdb )
    [OFDM_demod, OFDM_data_symb,ant] = ofdm_func.symb_transmission(data_symb, channelResponse,False, nonlinearity,OFDM_TX_piloto)
    equalized_Hest_ls = ofdm.equalizer(OFDM_demod, Hest_wcp)
    QAM_est = ofdm_func.symb_ber(data_symb, demapping_table,equalized_Hest_ls,bits_all)
    return QAM_est

def Exact(bits_piloto_all, bits_all, N, K, mu, mapping_table,demapping_table, SNRdb , nonlinearity, channelResponse):

    Hest_exact = np.fft.fft(channelResponse, N)  # H exato quando CP=L
    pilot_symb = ofdmclass.symbol_block(bits_piloto_all, N, K, mu, mapping_table, SNRdb)
    [OFDM_demod_piloto, OFDM_data_piloto, OFDM_TX_piloto]= ofdm_func.symb_transmission(pilot_symb, channelResponse, True, nonlinearity,[])

    data_symb = ofdmclass.symbol_block(bits_all, N, K, mu, mapping_table, SNRdb )
    [OFDM_demod, OFDM_data_symb,ant] = ofdm_func.symb_transmission(data_symb, channelResponse,False, nonlinearity,OFDM_TX_piloto)
    equalized_Hest_ls = ofdm.equalizer(OFDM_demod, Hest_exact)
    QAM_est = ofdm_func.symb_ber(data_symb, demapping_table,equalized_Hest_ls,bits_all)
    return QAM_est
