#!/usr/bin/env python
# coding: utf-8
import numpy as np
import pickle
import matplotlib
import matplotlib.pyplot as plt
import pathlib as pb
import os
import plotly.graph_objects as go
import numpy as np
import fig_lib2

################################################################################################################################
# This script is used to generate the BER curve by using the simulation results in /results_ber folder.
################################################################################################################################
#------------------------------------------------------------------------------------------------------------------------------#
# Going to results folder and selecting ber results files:

[SNRdb_zp0, SNRdb_zp5, SNRdb_zp10, ber_ls, ber_zf_mr, ber_wcp, ber_wcp_exact,  ber_net1_wcp, ber_net1_ncp, ber_net2_ncp, ber_net1_cp10, ber_net1_wzp,  ber_wzpzj, ber_net1_zp10] = fig_lib2.BER_SNR('results_ber')#------------------------------------------------------------------------------------------------------------------------------#
# Visualizing bit-error-rate vs SNR:
MM=12
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = ['Times New Roman'] + plt.rcParams['font.serif']
# plt.rcParams.update({'font.size': 18})

SMALL_SIZE = 18
MEDIUM_SIZE = 10
BIGGER_SIZE = 24

plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


fig = plt.figure(figsize=(12, 6))
plt.plot(SNRdb_zp0[0:7], ber_ls[0:7], marker='x', color='red',label='ZP: LS+BFDE (K=0)',markersize=MM)
plt.plot(SNRdb_zp5[0:7], ber_zf_mr[0:7], marker='d', color='gold',label='ZP: LS+BFDE (K=5)',markersize=MM)
plt.plot(SNRdb_zp10[0:7], ber_wcp_exact[0:7], marker='o', color='green',label='ZP: Exact+BFDE (K=10)',markersize=MM)
plt.plot(SNRdb_zp10[0:7], ber_wcp[0:7], marker='s', color='deepskyblue',label='ZP: LMMSE+BFDE (K=10)',markersize=MM)
plt.plot(SNRdb_zp5[0:7],  ber_net1_wzp[0:7],  marker='*',  linestyle='dashed', color='crimson',label='ZPZJ: ICE+reg (K=5)',markersize=MM+3)
plt.plot(SNRdb_zp5[0:7],  ber_wzpzj[0:7],  marker='*',  linestyle='dashed',color='blue',label='ZPZJ: LS+reg (K=5)',markersize=MM+3)
plt.plot(SNRdb_zp5[0:7],  ber_net1_wcp[0:7],  marker='^', color='black',label='ZP: ICE+BFDE (K=5)',markersize=MM)
plt.plot(SNRdb_zp0[0:7],  ber_net1_ncp[0:7], marker='^', color='purple',label='ZP: ICE+BFDE (K=0)',markersize=MM)
plt.plot(SNRdb_zp0[0:7],  ber_net2_ncp[0:7], marker='v', color='hotpink',label='ZP: ICE+SDMR (K=0)',markersize=MM)
plt.plot(SNRdb_zp10[0:7],  ber_net1_cp10[0:7], marker='^', color='darkorange',label='ZP: ICE+BFDE (K=10)',markersize=MM)
plt.plot(SNRdb_zp10[0:7],  ber_net1_zp10[0:7],  marker='*',  linestyle='dashed', color='darkred',label='ZPZJ: ICE+MMSE (K=10)',markersize=MM+3)

# plt.legend(framealpha=1, frameon=True, ncol =2);
from matplotlib.font_manager import FontProperties
fontP = FontProperties()
fontP.set_size('small')
plt.legend(bbox_to_anchor=(1, 1), loc='upper left', prop=fontP)
plt.xscale('linear')
plt.yscale('log')
# plt.xlabel('$S(\\E_b/N_0)$')
plt.xlabel('Eb/N0 (dB)')
plt.ylabel('BER')
plt.grid(True, which='minor',linestyle=':')
plt.grid(True, which='major')
plt.ylim((10**(-5), 1))
plt.subplots_adjust(right=0.7)
plt.show()

output_filename = 'ber'
fig.savefig(output_filename+'.png')
fig.savefig(output_filename+'.pdf')
################################################################################################################################



